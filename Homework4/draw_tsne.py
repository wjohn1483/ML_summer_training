import scipy.misc
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
from keras.datasets import mnist
from keras.models import model_from_json
from keras import backend as K
from sklearn.manifold import TSNE

task = 'autoencoder_encodeDim128_withoutNoise'
architecture_file = './weights_' + task + '/dnn_' + task + '_architecture.json'
weight_file = './weights_' + task + '/dnn_' + task + '.h5'
output_dir = 'img/'

# Get clean data
(X_train, y_train), (X_test, y_test) = mnist.load_data()

X_train = X_train.reshape(60000, 784)
X_test = X_test.reshape(10000, 784)
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255

# Load model
model = model_from_json(open(architecture_file).read())
model.load_weights(weight_file)
model.compile(loss='binary_crossentropy', optimizer='adadelta', metrics=['accuracy'])
encoder = K.function([model.layers[0].input], [model.layers[2].output])
model.summary()

# Convert output of bottleneck layer of autoencoder to 2 dimension array
print("Converting to 2 dimension...")
tsneModel = TSNE(n_components=2, random_state=0)
np.set_printoptions(suppress=True)
code = tsneModel.fit_transform(encoder([X_test])[0])

# Plot the data
print("Plotting...")
plt.figure(figsize=(6, 6))
plt.scatter(code[:, 0], code[:, 1], c=y_test)
plt.colorbar()
plt.savefig("img/code_plot.png")

print("Done")
