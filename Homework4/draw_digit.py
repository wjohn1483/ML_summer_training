#import matplotlib.pyplot as plt
import scipy.misc
import numpy as np
#from pylab import figure, axes, pie, title, show
from keras.datasets import mnist
from keras.models import model_from_json
from keras import backend as K

task = 'autoencoder_encodeDim128_withoutNoise'
architecture_file = './weights_' + task + '/dnn_' + task + '_architecture.json'
weight_file = './weights_' + task + '/dnn_' + task + '.h5'
output_dir = 'img/'
numberOfImage = 10
encodeDim = 128

# Get clean data
(X_train, y_train), (X_test, y_test) = mnist.load_data()

X_train = X_train.reshape(60000, 784)
X_test = X_test.reshape(10000, 784)
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255

## Get Noisy data
#(x_train, _), (x_test, _) = mnist.load_data()
#x_train = x_train.reshape(60000, 784)
#x_test = x_test.reshape(10000, 784)
#x_train = x_train.astype('float32') / 255.
#x_test = x_test.astype('float32') / 255.
#
## add noise
#noise_factor = 0.5
#x_train_noisy = x_train + noise_factor * np.random.normal(loc=0.0, scale=1.0, size=x_train.shape) 
#x_test_noisy = x_test + noise_factor * np.random.normal(loc=0.0, scale=1.0, size=x_test.shape) 
#
#X_train_noisy = np.clip(x_train_noisy, 0., 1.)
#X_test = np.clip(x_test_noisy, 0., 1.)

# Load model
model = model_from_json(open(architecture_file).read())
model.load_weights(weight_file)
model.compile(loss='binary_crossentropy', optimizer='adadelta', metrics=['accuracy'])
reconstruct_output = K.function([model.layers[3].input], [model.layers[3].output])
model.summary()

for i in range(0, numberOfImage):
  randomCode = np.random.normal(0, 1, encodeDim)
  #scipy.misc.imsave(output_dir + 'reconstruct_image' + str(i) + '.png', reconstruct_output([randomCode.reshape(1, 128)])[0].reshape(28, 28))
  scipy.misc.imsave(output_dir + 'reconstruct_image' + str(i) + '.png', model.predict(X_test[i].reshape(1, 784)).reshape(28, 28))


print("Done")
