from __future__ import print_function
import sys
import numpy as np
#np.random.seed(1337)  # for reproducibility

from keras.datasets import mnist
from keras.models import Sequential, Model
from keras.layers import Dense, Dropout, Activation, Input
from keras.optimizers import SGD, Adam, RMSprop
from keras.utils import np_utils
from keras.regularizers import ActivityRegularizer

batch_size = 128
nb_classes = 10
nb_epoch = 50
learning_rate = 0.008
train_title = sys.argv[1]
weight_file = 'weights_' + train_title + '/dnn_' + train_title + '.h5'
architecture_file = 'weights_' + train_title + '/dnn_' + train_title + '_architecture.json'
accuracy_file = 'weights_' + train_title + '/dnn_' + train_title + '_training_accuracy.txt'

# Clean data
(X_train, y_train), (X_test, y_test) = mnist.load_data()

X_train = X_train.reshape(60000, 784)
X_test = X_test.reshape(10000, 784)
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')

# Noisy data
#(x_train, _), (x_test, _) = mnist.load_data()
#x_train = x_train.reshape(60000, 784)
#x_test = x_test.reshape(10000, 784)
#x_train = x_train.astype('float32') / 255.
#x_test = x_test.astype('float32') / 255.
#
## add noise
#noise_factor = 0.5
#x_train_noisy = x_train + noise_factor * np.random.normal(loc=0.0, scale=1.0, size=x_train.shape) 
#x_test_noisy = x_test + noise_factor * np.random.normal(loc=0.0, scale=1.0, size=x_test.shape) 
#
#X_train_noisy = np.clip(x_train_noisy, 0., 1.)
#X_test_noisy = np.clip(x_test_noisy, 0., 1.)

# Model
model = Sequential()
input_feature = Input(shape=(784, ))
inputLayer = Dense(784, activation='sigmoid')(input_feature)
encodingLayer = Dense(128, activation='sigmoid', activity_regularizer=ActivityRegularizer(l2=0.001))(inputLayer)
outputLayer = Dense(784, activation='linear')(encodingLayer)
model = Model(input=input_feature, output=outputLayer)
#model.summary()
#opt=SGD(lr=learning_rate)
model.compile(loss='mse',
              optimizer='adamax',
              metrics=['accuracy'])

history = model.fit(X_train, X_train,
                    batch_size=batch_size, nb_epoch=nb_epoch, shuffle=True,
                    verbose=1)#, validation_data=(X_test, X_test))
score = model.evaluate(X_test, X_test, verbose=0)

# Save model
json_string = model.to_json()
open(architecture_file, 'w').write(json_string)
model.save_weights(weight_file, overwrite=True)
  
accuracyFilePointer = open(accuracy_file, 'w')
accuracyFilePointer.write("Score : " + str(score[0]) + '\n')
accuracyFilePointer.write("Accuracy : " + str(score[1]))

print('Test score:', score[0])
print('Test accuracy:', score[1])

