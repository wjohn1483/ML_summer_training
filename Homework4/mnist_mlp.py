'''Trains a simple deep NN on the MNIST dataset.

Gets to 98.40% test accuracy after 20 epochs
(there is *a lot* of margin for parameter tuning).
2 seconds per epoch on a K520 GPU.
'''

from __future__ import print_function
import numpy as np
import sys
np.random.seed(1337)  # for reproducibility

from keras.datasets import mnist
from keras.models import Sequential, model_from_json
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import SGD, Adam, RMSprop
from keras.utils import np_utils
from keras import backend as K


batch_size = 128
nb_classes = 10
nb_epoch = 50
learning_rate = 0.008
train_title = sys.argv[1]
weight_file = 'weights_' + train_title + '/dnn_' + train_title + '.h5'
architecture_file = 'weights_' + train_title + '/dnn_' + train_title + '_architecture.json'
accuracy_file = 'weights_' + train_title + '/dnn_' + train_title + '_training_accuracy.txt'
autoencoder_title = 'autoencoder_encodeDim128_withoutNoise'
autoencoder_weight_file = 'weights_' + autoencoder_title + '/dnn_' + autoencoder_title + '.h5'
autoencoder_architecture_file = 'weights_' + autoencoder_title + '/dnn_' + autoencoder_title + '_architecture.json'

# the data, shuffled and split between train and test sets
(X_train, y_train), (X_test, y_test) = mnist.load_data()

X_train = X_train.reshape(60000, 784)
X_test = X_test.reshape(10000, 784)
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')

## Noisy data
#(x_train, y_train), (x_test, y_test) = mnist.load_data()
#x_train = x_train.reshape(60000, 784)
#x_test = x_test.reshape(10000, 784)
#x_train = x_train.astype('float32') / 255.
#x_test = x_test.astype('float32') / 255.
#
## add noise
#noise_factor = 0.5
#x_train_noisy = x_train + noise_factor * np.random.normal(loc=0.0, scale=1.0, size=x_train.shape) 
#x_test_noisy = x_test + noise_factor * np.random.normal(loc=0.0, scale=1.0, size=x_test.shape) 
#
#X_train = np.clip(x_train_noisy, 0., 1.)
#X_test = np.clip(x_test_noisy, 0., 1.)

# convert class vectors to binary class matrices
Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)

# Load autoencoder
autoencoder = model_from_json(open(autoencoder_architecture_file).read())
autoencoder.load_weights(autoencoder_weight_file)
encoder_output = K.function([autoencoder.layers[0].input], [autoencoder.layers[2].output])
autoencoder.summary()

# Model  
model = Sequential()
model.add(Dense(512, input_shape=(128,)))
model.add(Activation('relu'))
model.add(Dropout(0.2))
model.add(Dense(512))
model.add(Activation('relu'))
model.add(Dropout(0.2))
model.add(Dense(10))
model.add(Activation('softmax'))

#model.summary()
opt = SGD(lr=learning_rate)

model.compile(loss='categorical_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])

history = model.fit(encoder_output([X_train])[0], Y_train,
                    batch_size=batch_size, nb_epoch=nb_epoch, shuffle=True,
                    verbose=1)#, validation_data=(X_test, Y_test))
score = model.evaluate(encoder_output([X_test])[0], Y_test, verbose=0)
#history = model.fit(X_train, Y_train, batch_size=batch_size, nb_epoch=nb_epoch, shuffle=True, verbose=1)
#score = model.evaluate(X_test, Y_test, verbose=1)

# Save model
json_string = model.to_json()
open(architecture_file, 'w').write(json_string)
model.save_weights(weight_file, overwrite=True)
  
accuracyFilePointer = open(accuracy_file, 'w')
accuracyFilePointer.write("Score : " + str(score[0]) + '\n')
accuracyFilePointer.write("Accuracy : " + str(score[1]))
print('Test score:', score[0])
print('Test accuracy:', score[1])
